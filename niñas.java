
import java.util.Date;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author sistemas
 */
public class ninhas {
    
    public String nombre;
    public String color;
    public byte tamanio;
    public String raza;
    public int edad;
    public boolean orejasGrandes;
    public boolean brillosa;
    public String comportamiento;
    public Date fechaNacimiento;

    public ninhas(String color, byte tamanio, String raza, byte edad, boolean orejasGrandes, boolean brillosa, String comportamiento) {
        this.color = color;
        this.tamanio = tamanio;
        this.raza = raza;
        this.edad = edad;
        this.orejasGrandes = orejasGrandes;
        this.brillosa = brillosa;
        this.comportamiento = comportamiento;
    }

    ninhas() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    public String jugar(){
        return "estoy jugando";
            }
    
 

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public byte getTamanio() {
        return tamanio;
    }

    public void setTamanio(byte tamanio) {
        this.tamanio = tamanio;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(byte edad) {
        this.edad = edad;
    }

    public boolean isOrejasGrandes() {
        return orejasGrandes;
    }

    public void setOrejasGrandes(boolean orejasGrandes) {
        this.orejasGrandes = orejasGrandes;
    }

    public boolean isBrillosa() {
        return brillosa;
    }

    public void setBrillosa(boolean brillosa) {
        this.brillosa = brillosa;
    }

    public String getComportamiento() {
        return comportamiento;
    }

    public void setComportamiento(String comportamiento) {
        this.comportamiento = comportamiento;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    
    
            
}

class maya {
    
    public static void main(String [] args){
        
        ninhas maya = new ninhas();
        maya.setBrillosa(true);
        maya.setColor("dorada");
        maya.setComportamiento("jodida");
        maya.setRaza("no");
        maya.setNombre("mayita");
        maya.setOrejasGrandes(true);
        System.out.println(maya.getColor());
      
        
        
    }
}
