public class gato {

    //	DEFINIMOS LOS ATRIBUTOS DE NUESTRO GATO
	private String nombre;
	private String color;
	private int peso;
	private int edad;

    //	METODO CONSTRUCTOR DE NUESTRA CLASE GATO
	public gato() {
		
		color="gris";
		peso=10;
		edad=4;
		
    }

//	METODO PARA PONERLE UN NOMBRE A EL GATO
	public void ponleNombre(String nombreGato) {
		nombre=nombreGato;
			
	}

    //	METODO PARA QUE LA INFORMACION DE NUESTRO GATO APAREZCA EN CONSOLA
	public String dimeInformacion() {
		
		return "Hola! me llamo " + nombre+", soy de color "+color+", peso "+peso+ " kilos"+" y tengo "+edad+" años";
	}

    //	METODO PARA JUGAR
	public String jugar() {
		
		return "Estoy jugando y soy muy divertid@ :)";
		
	}

    //	METODO PARA RASGUÑAR
	public String rasguniar() {
		
		return "Estoy rasguñando pq estoy estresad@";
	}

    //	METODO PARA RONRONEAR
	public String ronronear() {
		
		return "rrrrrrrrr";
	}

    //	METODO PARA COMER
	public String comer() {
		
		return "Tengo hambre, estoy comiendo.";
	}

    //	METODO PARA DECIR Q ESTA ASUSTADO
	public String asustado() {
		
		return nombre+ "esta asustad@, ira a esconderse.";
	}
}